package com.workjam.workjam.features.chat;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public abstract class ReactFragment<T extends ReactComponent> extends Fragment {
    private final int OVERLAY_PERMISSION_REQ_CODE = 1;  // Choose any value

    @Nullable
    abstract T getReactComponent();

    abstract ListenerManager<ActivityEventListener> getActivityListenerManager();

    abstract ListenerManager<ActivityLifecycleListener> getmLifecycleListenerManager();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(getContext())) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getActivity().getPackageName()));
                startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivityListenerManager().register(getReactComponent());
        getmLifecycleListenerManager().register(getReactComponent());
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivityListenerManager().register(getReactComponent());
        getmLifecycleListenerManager().register(getReactComponent());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == OVERLAY_PERMISSION_REQ_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.canDrawOverlays(getContext())) {
                    // SYSTEM_ALERT_WINDOW permission not granted
                }
            }
        }
        getReactComponent().onActivityResult(getActivity(), requestCode, resultCode, data);
    }
}

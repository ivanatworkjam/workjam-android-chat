package com.workjam.workjam.features.chat;

import android.app.Activity;

import org.jetbrains.annotations.NotNull;

public interface ActivityLifecycleListener {

    void onActivityCreate(@NotNull Activity activity);

    void onStart(@NotNull Activity activity);

    void onResume(@NotNull Activity activity);

    void onPause(@NotNull Activity activity);

    void onStop(@NotNull Activity activity);

    void onDestroy(@NotNull Activity activity);
}

package com.workjam.workjam.features.chat;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.Arrays;

class WorkJamReactAuthModule extends ReactContextBaseJavaModule {

    private final ReactAccountProvider mReactAccountProvider;

    WorkJamReactAuthModule(ReactApplicationContext reactContext, ReactAccountProvider reactAccountProvider) {
        super(reactContext);
        mReactAccountProvider = reactAccountProvider;
    }

    @Override
    public String getName() {
        return "ReactNativeEventEmitter";
    }

    @ReactMethod
    public void getAuthData(Callback reactCallback) {
        mReactAccountProvider.fetchAccount(reactCallback);
    }

    @ReactMethod
    public void subrouteOff() {
        // TODO: replace the navbar title with the default "Messages"
    }

    @ReactMethod
    public void subrouteOn(String title) {
        // TODO: set the navbar title to "title"
    }

    @ReactMethod
    public void setTitle(String title) {
        // STUB: for iOS compatibility and reserved for future use
    }
}

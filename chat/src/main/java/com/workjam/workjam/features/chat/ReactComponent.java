package com.workjam.workjam.features.chat;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

public interface ReactComponent extends ActivityEventListener, ActivityLifecycleListener {
    View getView();
    void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data);
}

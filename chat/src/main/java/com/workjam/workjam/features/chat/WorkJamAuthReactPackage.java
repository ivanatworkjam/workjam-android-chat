package com.workjam.workjam.features.chat;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class WorkJamAuthReactPackage implements ReactPackage {
    private final ReactAccountProvider mReactAccountProvider;

    WorkJamAuthReactPackage(ReactAccountProvider reactAccountProvider) {
        mReactAccountProvider = reactAccountProvider;
    }

    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
        List<NativeModule> nativeModules = new ArrayList<>();
        nativeModules.add(new WorkJamReactAuthModule(reactContext, mReactAccountProvider));
        return nativeModules;
    }

    @Override
    public List<ViewManager> createViewManagers(
            ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }
}

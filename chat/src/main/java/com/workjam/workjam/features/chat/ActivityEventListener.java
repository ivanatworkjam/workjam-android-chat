package com.workjam.workjam.features.chat;

import android.support.annotation.NonNull;
import android.view.KeyEvent;

public interface ActivityEventListener {

    void onKeyUp(int keyCode, @NonNull KeyEvent event);

    boolean onBackPressed();
}

package com.workjam.workjam.features.chat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bitgo.randombytes.RandomBytesPackage;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.common.LifecycleState;
import com.facebook.react.shell.MainReactPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.rnfs.RNFSPackage;
import com.workjam.android.chat.BuildConfig;
import com.zmxv.RNSound.RNSoundPackage;

import org.jetbrains.annotations.NotNull;
import org.pgsqlite.SQLitePluginPackage;

import javax.inject.Inject;

class ReactReactComponent implements ReactComponent, ActivityEventListener, ActivityLifecycleListener {
    private Activity mActivity;
    private ReactRootView mReactRootView;
    private ReactInstanceManager mReactInstanceManager;

    @Inject
    ReactReactComponent(
            Context context,
            AppCompatActivity activity,
            ReactAccountProvider reactAccountProvider) {

        mActivity = activity;
        mReactRootView = new ReactRootView(context);
        mReactInstanceManager = ReactInstanceManager.builder().
                setApplication(activity.getApplication())
                .setCurrentActivity(activity)
                .setBundleAssetName("index.android.bundle")
                .setJSMainModulePath("index.android")
                .addPackage(new MainReactPackage())
                .addPackage(new RandomBytesPackage())
                .addPackage(new RNDeviceInfo())
                .addPackage(new RNFSPackage())
                .addPackage(new RNSoundPackage())
                .addPackage(new SQLitePluginPackage())
                .addPackage(new RNSodiumPackage())
                .addPackage(new RNKeychainPackage())
                .addPackage(new WorkJamAuthReactPackage(reactAccountProvider))
                .setUseDeveloperSupport(BuildConfig.DEBUG)
                .setInitialLifecycleState(LifecycleState.RESUMED)
                .build();
        mReactRootView.startReactApplication(mReactInstanceManager, "peeriomobile", null);
    }

    @Override
    public void onKeyUp(int keyCode, @NonNull KeyEvent event) {
        if (keyCode == 83 && mReactInstanceManager != null) {
            mReactInstanceManager.showDevOptionsDialog();
        }
    }

    @Override
    public boolean onBackPressed() {
        // if yes, we consume all back button presses in the mReactInstanceManager
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onBackPressed();
            return true;
        }
        return false;
    }

    @Override
    public View getView() {
        return mReactRootView;
    }

    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        mReactInstanceManager.onActivityResult(activity, requestCode, resultCode, data);
    }

    @Override
    public void onActivityCreate(@NotNull Activity activity) {

    }

    @Override
    public void onStart(@NotNull Activity activity) {

    }

    @Override
    public void onResume(@NotNull Activity activity) {
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(activity);
        }
    }

    @Override
    public void onPause(@NotNull Activity activity) {
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause(mActivity);
        }
    }

    @Override
    public void onStop(@NotNull Activity activity) {

    }

    @Override
    public void onDestroy(@NotNull Activity activity) {
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostDestroy(mActivity);
        }
        if (mReactRootView != null) {
            mReactRootView.unmountReactApplication();
        }
    }
}

package com.workjam.workjam.features.chat;

import androidx.annotation.NonNull;

public interface ListenerManager<T> {

    void register(@NonNull T listener);

    void unregister(@NonNull T listener);
}

package com.workjam.workjam.features.chat;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Callback;

interface ReactAccountProvider {

    void fetchAccount(@NonNull Callback reactCallback);

    void dispose();
}

#!/usr/bin/env bash
yarn
echo "Copying JS bundle to workjam/src/main/assets"
cp ../node_modules/\@workjam/wj-android-chat/dist/index.android.bundle* ../chat/src/main/assets/
exit 0